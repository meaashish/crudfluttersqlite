import 'package:flutter/material.dart';

const String routeLoginPage= '/login';
const String routeRegisterPage = '/register';
const String routeNewPage = '/newpage';
const String routeForgotPasswordPage = '/forgotpassword';
const String routeInputPage = '/takeinput';
// const String routeAbout = '/about';
// const String routeContacts = '/contact';

final navKey = new GlobalKey<NavigatorState>();