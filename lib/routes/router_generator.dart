import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app/pages/loginpage.dart';
import 'package:my_app/pages/newscreen.dart';
import 'package:my_app/pages/registerpage.dart';
import 'package:my_app/routes/routes.dart';
import 'package:my_app/pages/forgot_password_page.dart';
import 'package:my_app/pages/inputPage.dart';
// import 'package:my_app/pages/newscreen.dart';
// import 'package:my_app/ui/contact_page.dart';
// import 'package:my_app/ui/home_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case routeLoginPage:
        return MaterialPageRoute(builder: (_) => LoginPage());
        break;
      case routeRegisterPage:
        return MaterialPageRoute(builder: (_) => RegisterPage());
        break;
      case routeNewPage:
        return MaterialPageRoute(builder: (_) => NewScreen());
        break;
      case routeForgotPasswordPage:
        return MaterialPageRoute(builder: (_) => ForgotPassword());
        break;
      case routeInputPage:
        return MaterialPageRoute(builder: (_) => InputPage());
        break;
      // case routeContacts:
      //   return MaterialPageRoute(builder: (_) => ContactPage());
      // break;
    }
  }
}
