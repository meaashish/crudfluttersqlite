import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:my_app/models/user.dart';

class DatabaseHelper {
  static const _dbName = 'UserData.db';
  static const _dbVersion = 1;

  //singleton class
  DatabaseHelper._();
  static final DatabaseHelper instance = DatabaseHelper._();

  Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    Directory dataDirectory = await getApplicationDocumentsDirectory();
    print(dataDirectory);
    String dbPath = join(dataDirectory.path, _dbName);
    print(dbPath);
    return await openDatabase(dbPath,
        version: _dbVersion, onCreate: _onCreateDB);
  }

  _onCreateDB(Database db, int version) async {
    await db.execute("CREATE TABLE users ("
        "id INTEGER PRIMARY KEY,"
        "name TEXT,"
        "email TEXT,"
        "password TEXT"
        ")");
  }

  Future<int> insertUser(User user) async {
    Database db = await database;
    return await db.insert(User.tblUser, user.toMap());
  }

  Future<int> updateUser(User user) async {
    Database db = await database;
    return await db.update(User.tblUser, user.toMap(),
        where: 'id=?', whereArgs: [user.id]);
  }

  Future<int> deleteUser(int id) async {
    Database db = await database;
    return await db.delete(User.tblUser, where: 'id = ?', whereArgs: [id]);
  }

  Future<List<User>> fetchUser() async {
    Database db = await database;
    List<Map> users = await db.query(User.tblUser);
    return users.length == 0 ? [] : users.map((e) => User.fromMap(e)).toList();
  }
}
