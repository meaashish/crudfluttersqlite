import 'package:flutter/material.dart';
import 'package:my_app/utils/validator.dart';
import 'package:my_app/routes/routes.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  var _formKey = GlobalKey<FormState>();
  var isLoading = false;

  void _submit() {
    final isValid = _formKey.currentState.validate();
    // if (!isValid) {
    //   return;
    // }
    // Navigator.pushNamed(context, routeNewPage);
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      autofocus: false,
      validator: validateEmail,
      onSaved: (value) => {},
      decoration: InputDecoration(hintText: 'Enter Email'),
    );

    final submitButton = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RaisedButton(
            color: Colors.blue[600],
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 15.0,
            ),
            child: Text(
              "Submit",
              style: TextStyle(fontSize: 24.0, color: Colors.white),
            ),
            onPressed: () => _submit(),
          )
        ]);

    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(40.0),
          // width: 500,
          child: Form(
              key: _formKey,
              child: Center(
                child: Column(
                  children: <Widget>[
                    // Image.asset(
                    //   'images/logo.png',
                    //   width: 200.0,
                    //   fit: BoxFit.cover,
                    // ),
                    SizedBox(height: 15.0),
                    SizedBox(height: 5.0),
                    emailField,
                    SizedBox(height: 5.0),
                    submitButton,
                    // loading
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
