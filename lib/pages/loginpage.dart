import 'package:flutter/material.dart';
import 'package:my_app/utils/validator.dart';
import 'package:my_app/routes/routes.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  var _formKey = GlobalKey<FormState>();
  var isLoading = false;

  void _login() {
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    Navigator.pushNamed(context, routeNewPage);
  }

  @override
  Widget build(BuildContext context) {
    final usernameField = TextFormField(
      autofocus: false,
      validator: validateEmail,
      onSaved: (value) => {},
      decoration: InputDecoration(hintText: 'Enter Email'),
    );

    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      validator: (value) => value.isEmpty ? "Please enter password" : null,
      onSaved: (value) => {},
      decoration: InputDecoration(hintText: 'Enter password'),
    );

    // var loading = Row(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   children: <Widget>[
    //     CircularProgressIndicator(),
    //     Text(" Authenticating ... Please wait")
    //   ],
    // );

    final loginButton = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RaisedButton(
            color: Colors.blue[600],
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 15.0,
            ),
            child: Text(
              "Login",
              style: TextStyle(fontSize: 24.0, color: Colors.white),
            ),
            onPressed: () => _login(),
          )
        ]);

    final forgotLabel = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FlatButton(
          padding: EdgeInsets.all(0.0),
          child: Text("Forgot password?",
              style: TextStyle(fontWeight: FontWeight.w300)),
          onPressed: () {
            Navigator.pushNamed(context, routeForgotPasswordPage);
          },
        ),
        FlatButton(
          padding: EdgeInsets.only(left: 0.0),
          child: Text("Sign up", style: TextStyle(fontWeight: FontWeight.w300)),
          onPressed: () {
            Navigator.pushNamed(context, routeRegisterPage);
          },
        ),
      ],
    );

    return MaterialApp(
        home: new Scaffold(
      body: Container(
          child: new Column(children: [
        new Container(
          padding: EdgeInsets.all(40.0),
          child: Form(
              key: _formKey,
              child: Center(
                child: Column(
                  children: <Widget>[
                    // Image.asset(
                    //   'images/logo.png',
                    //   width: 200.0,
                    //   fit: BoxFit.cover,
                    // ),
                    SizedBox(height: 15.0),
                    SizedBox(height: 5.0),
                    usernameField,
                    SizedBox(height: 5.0),
                    passwordField,
                    SizedBox(height: 5.0),
                    forgotLabel,
                    SizedBox(height: 5.0),
                    loginButton,
                    // loading
                  ],
                ),
              )),
        ),
      ])),
    ));
  }
}
