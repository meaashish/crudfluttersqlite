import 'package:flutter/material.dart';
import 'package:my_app/utils/database_helper.dart';
import 'package:my_app/utils/validator.dart';
import 'package:my_app/pages/loginpage.dart';
import 'package:my_app/models/user.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  User _user = User();
  final _formKey = GlobalKey<FormState>();

  List<User> _users = [];

  DatabaseHelper _dbHelper;

  final _ctrlName = TextEditingController();
  final _ctrlEmail = TextEditingController();
  final _ctrlPassword = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      _dbHelper = DatabaseHelper.instance;
    });
    _refreshUserList();
  }

  @override
  Widget build(BuildContext context) {
    final usernameField = TextFormField(
      autofocus: false,
      validator: (value) => value.isEmpty ? "This field is required" : null,
      onSaved: (value) => setState(() => _user.name = value),
      controller: _ctrlName,
      decoration: InputDecoration(hintText: 'Username'),
    );

    final emailField = TextFormField(
      autofocus: false,
      validator: validateEmail,
      onSaved: (value) => setState(() => _user.email = value),
      controller: _ctrlEmail,
      decoration: InputDecoration(hintText: 'Email'),
    );

    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      validator: (value) => value.isEmpty ? "This field is required" : null,
      onSaved: (value) => setState(() => _user.password = value),
      controller: _ctrlPassword,
      decoration: InputDecoration(hintText: 'password'),
    );

    // var loading = Row(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   children: <Widget>[
    //     CircularProgressIndicator(),
    //     Text(" Authenticating ... Please wait")
    //   ],
    // );

    final listView = Expanded(
      child: Card(
        margin: EdgeInsets.fromLTRB(0.5, 0.5, 0.5, 0.5),
        child: ListView.builder(
          itemBuilder: (context, index) {
            return Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    Icons.account_circle,
                    color: Colors.red,
                    size: 40.0,
                  ),
                  title: Text(_users[index].name),
                  subtitle: Text(_users[index].email),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () async {
                      await _dbHelper.deleteUser(_users[index].id);
                      _resetForm();
                      _refreshUserList();
                    },
                  ),
                  onTap: () {
                    setState(() {
                      _user = _users[index];
                      _ctrlName.text = _user.name;
                      _ctrlEmail.text = _user.email;
                      _ctrlPassword.text = _user.password;
                    });
                  },
                ),
                Divider(
                  height: 5.0,
                ),
              ],
            );
          },
          itemCount: _users.length,
        ),
      ),
    );

    final submitButton = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RaisedButton(
            color: Colors.blue[600],
            padding: EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 15.0,
            ),
            child: Text(
              "Submit",
              style: TextStyle(fontSize: 24.0, color: Colors.white),
            ),
            onPressed: () => _submit(),
          )
        ]);

    final alreadyAccounttLabel = Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FlatButton(
            padding: EdgeInsets.all(0.0),
            child: Text("Already have account ?",
                style: TextStyle(fontWeight: FontWeight.w300)),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => LoginPage()));
            },
          )
        ]);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "My App",
            textAlign: TextAlign.center,
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(40.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                // Image.asset(
                //   'images/logo.png',
                //   width: 200.0,
                //   fit: BoxFit.cover,
                // ),
                SizedBox(height: 15.0),
                SizedBox(height: 5.0),
                usernameField,
                SizedBox(height: 5.0),
                emailField,
                SizedBox(height: 5.0),
                passwordField,
                SizedBox(height: 5.0),
                alreadyAccounttLabel,
                SizedBox(height: 5.0),
                submitButton,
                SizedBox(height: 15.0),
                listView,
              ],
            ),
          ),
        ),
      ),
    );
  }

  _refreshUserList() async {
    List<User> x = await _dbHelper.fetchUser();
    setState(() {
      _users = x;
    });
  }

  _submit() async {
    var form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      if (_user.id == null)
        await _dbHelper.insertUser(_user);
      else
        await _dbHelper.updateUser(_user);
      _refreshUserList();
      _resetForm();
    }
  }

  _resetForm() {
    setState(() {
      _formKey.currentState.reset();
      _ctrlName.clear();
      _ctrlEmail.clear();
      _ctrlPassword.clear();
      _user.id = null;
    });
  }
}
